package main

import (
	ddv "git.scc.kit.edu/KIT-CA/dfn-domain-validation"
	"log"
	"os"
)

func main() {
	// parse email from stdin
	url, domain, err := ddv.GetEmailData(os.Stdin)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Got URL %s for domain %s", url, domain)

	// click activation button
	err = ddv.ActivateDomain(ddv.DefaultWebclient, url)
	if err != nil {
		log.Printf("Error activiating %s: %s", domain, err)
	} else {
		log.Printf("Domain %s activated", domain)
	}
}
