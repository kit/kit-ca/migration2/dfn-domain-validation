package dfn_domain_validation

import "testing"

/*
func TestActivateDomain(t *testing.T) {
	//err := ActivateDomain(DefaultWebclient, "https://pki.pca.dfn.de/Eva/domain/kit-ca-g2/f8e3c348-74c1-4f11-991b-eb395bdb4723")
	err := ActivateDomain(DefaultWebclient, "https://pki.pca.dfn.de/Eva/domain/kit-ca-g2/d466743e-b995-430f-9aa8-6080e3e2ee2f")
	if err != nil {
		t.Fatal(err)
	}
}
*/

func TestExtractRATokenFromURL(t *testing.T) {
	raid, token := ExtractRATokenFromURL("https://pki.pca.dfn.de/Eva/domain/kit-ca-g2/f8e3c348-74c1-4f11-991b-eb395bdb4723")
	if raid != "kit-ca-g2" {
		t.Errorf("Wrong RA-ID: %s != %s", raid, "kit-ca-g2")
	}
	if token != "f8e3c348-74c1-4f11-991b-eb395bdb4723" {
		t.Errorf("Wrong token: %s != %s", token, "f8e3c348-74c1-4f11-991b-eb395bdb4723")
	}
}
