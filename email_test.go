package dfn_domain_validation

import (
	"os"
	"testing"
)

var (
	emaildatacases = []struct {
		Filename string
		Link     string
		Domain   string
	}{
		{
			Filename: "dumps/email/validation_example_01_server_energy-trans.de.eml",
			Link:     "https://pki.pca.dfn.de/Eva/domain/kit-ca-g2/3ae8037a-2c68-4ef6-a460-77cae7232bd6",
			Domain:   "energy-trans.de",
		},
		{
			Filename: "dumps/email/validation_example_02_server_bmbf.innovationsforum-wasserwirtschaft.de.eml",
			Link:     "https://pki.pca.dfn.de/Eva/domain/kit-ca-g2/d279dc88-1ada-473f-bef8-ea565b3ecd93",
			Domain:   "bmbf.innovationsforum-wasserwirtschaft.de",
		},
		{
			Filename: "dumps/email/validation_example_03_email_dvgw-ebi.de.eml",
			Link:     "https://pki.pca.dfn.de/Eva/domain/kit-ca-g2/4c601d94-2f07-45c6-be0e-0608a53c3c03",
			Domain:   "dvgw-ebi.de",
		},
	}
)

func TestGetEmailData(t *testing.T) {
	for _, testcase := range emaildatacases {
		email, err := os.Open(testcase.Filename)
		if err != nil {
			t.Fatal(err)
		}
		defer email.Close()

		link, domain, err := GetEmailData(email)
		if err != nil {
			t.Errorf("GetEmailData returned an error: %s", err)
		}
		if domain != testcase.Domain {
			t.Errorf("Wrong domain: %s", domain)
		}

		if link != testcase.Link {
			t.Errorf("Wrong link: »%s« instead of»%s«", link, testcase.Link)
		}
	}
}
