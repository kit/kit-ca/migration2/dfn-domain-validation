package dfn_domain_validation

import (
	"bufio"
	"errors"
	"io"
	"io/ioutil"
	"mime"
	"mime/multipart"
	"net/mail"
	"regexp"
	"strings"
)

var (
	ReDomainFromBody         *regexp.Regexp
	ReActivationLinkFromBody *regexp.Regexp
	ErrNoActivationLink      = errors.New("unable to find activation link")
	ErrNoDomainFound         = errors.New("unable to find domain")
	ErrMissingData           = errors.New("did not find all required data")
)

func init() {
	ReActivationLinkFromBody = regexp.MustCompile(`(?s)https://pki\.pca\.dfn\.de/Eva/domain/[[:graph:]]+\b`)
	ReDomainFromBody = regexp.MustCompile(`(?s)weil Sie(?: für)? die Domain (?P<domain>[-a-zA-Z0-9._]+) `)
}

func decodeRFC2047(s string) string {
	decoded, err := (&mime.WordDecoder{}).DecodeHeader(s)
	if err != nil || len(decoded) == 0 {
		return s
	}
	return decoded
}

func GetEmailData(email io.Reader) (activationURL string, domain string, err error) {
	msg, err := mail.ReadMessage(bufio.NewReader(email))
	if err != nil {
		return activationURL, domain, err
	}

	/*
		subject := msg.Header.Get("Subject")
		if len(subject) == 0 {
			return ActivationLink, Domain, errors.New("subject empty")
		}
	*/

	mediaType, params, err := mime.ParseMediaType(msg.Header.Get("Content-Type"))
	if err != nil {
		return activationURL, domain, err
	}

	if strings.HasPrefix(mediaType, "multipart/") {
		mr := multipart.NewReader(msg.Body, params["boundary"])
		for {
			p, err := mr.NextPart()
			if err == io.EOF {
				break
			}
			if err != nil {
				return activationURL, domain, err
			}

			// decode any Q-encoded values
			/*
				for name, values := range p.Header {
					for idx, val := range values {
						fmt.Printf("%d: %s: %s\n", idx, name, decodeRFC2047(val))
					}
				}
			*/

			// text part of the message
			if strings.HasPrefix(p.Header.Get("Content-Type"), "text/plain") {
				messagePart, err := ioutil.ReadAll(p)
				if err != nil {
					return activationURL, domain, ErrNoActivationLink
				}

				// extract activation link
				urls := ReActivationLinkFromBody.FindStringSubmatch(string(messagePart))
				if urls == nil {
					return activationURL, domain, ErrNoActivationLink
				}
				activationURL = urls[0]

				// extract domain
				domains := ReDomainFromBody.FindStringSubmatch(string(messagePart))
				if len(domains) < 2 {
					return activationURL, domain, ErrNoDomainFound
				}
				domain = domains[1]
				return activationURL, domain, nil
			}
		}
	}

	return activationURL, domain, ErrMissingData
}
