This tool expects a domain validation email from the DFN PKI on stdin. Correct input will reactivate the domain using the DFN PKI link inside the email.
Check out [dfn-domain-validation-check](https://git.scc.kit.edu/KIT-CA/dfn-domain-validation-check) for a tool that automatically triggers these emails.

Here's how to setup this tool with [postfix](http://www.postfix.org/) and either [courier maildrop](https://www.courier-mta.org/maildrop/) or
[procmail](https://wiki.archlinux.org/index.php/Procmail):

# maildrop

1. Setup a Linux server, install postfix and maildrop.
2. Have postfix deliver email to maildrop (edit `/etc/postfix/main.cf`). See postfix's documentation for details:
```
mailbox_command = /usr/bin/maildrop -d ${USER}
```
3. Add a user `ca-domains`:
```
adduser --system --disabled-login --home /var/ca-domains ca-domains
```
4. Build `DomainReactivationClient` from this repo (either from source or download a build artifact) and install to `/usr/local/bin`:
```
git clone git@git.scc.kit.edu:KIT-CA/dfn-domain-validation.git
cd dfn-domain-validation/DomainReactivationClient
go build -ldflags="-s -w"
install DomainReactivationClient /usr/local/bin/
```
5. Edit `ca-domains`' `~/.mailfilter` (adjust paths accordingly):
```
MONTHDATE=`/bin/date +%Y-%m`

# create folders 
`test -d "$HOME/.local" || mkdir -p "$HOME/.local"`
`test -d "$HOME/Mails" || mkdir -p "$HOME/Mails"`

# enable logging
`test -d "$HOME/Logs" || mkdir -p "$HOME/Logs"`
logfile $HOME/Logs/$MONTHDATE.log

# Put a copy of the email into $HOME/Mails/
`test -d $HOME/Mails || mkdir -p $HOME/Mails`
cc "$HOME/Mails/$MONTHDATE"

# process domain (re-)activation requests
if  (( /^From:\s+dfnpki-mailsender-noreply@dfn-cert.de/ ) \
  && ( /^Subject:\s+.*DFN-PKI: Validierung/ ))
{
  flock "$HOME/.local/dfn-domain-validation" {
    to "| /usr/local/bin/DomainReactivationClient"
  }
}
```
1. Route copies of all domain revalidation emails to this user.

# procmail

Preface: `procmail` is [unmaintained since 2001](https://marc.info/?l=openbsd-ports&m=141634350915839&w=2). Please only use it if you cannot use `maildrop`.

1. Setup a Linux server, install postfix and procmail.
2. Have postfix deliver email to procmail (edit `/etc/postfix/main.cf`). See postfix's documentation for details:
```
# Add procmailrc support
mailbox_command = /usr/bin/procmail -a "$EXTENSION"
```
3. Add a user `ca-domains`:
```
adduser --system --disabled-login --home /var/ca-domains ca-domains
```
4. Install this tool (either from source or download a build artifact).
5. Edit `ca-domains`' `.procmailrc` (adjust paths accordingly):
```
HOME=/var/ca-domains
MONTH=`/bin/date +%Y-%m`
USER=ca-domains

LOGFILE=$HOME/logs/${MONTH}
LOGABSTRACT=all
VERBOSE=no

# Put a copy of the mail into $HOME/Mail/INBOX.${MONTH}
:0c:
$HOME/Mail/${MONTH}

# process domain (re-)activation requests
:0:DomainReactivationClient.lock
* ^From.*dfnpki-mailsender-noreply@dfn-cert.de
* ^Subject:.*Validierung
| /var/ca-domains/src/dfn-domain-validation/DomainReactivationClient/DomainReactivationClient
```
1. Route copies of all domain revalidation emails to this user.
