package dfn_domain_validation

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"

	"golang.org/x/net/publicsuffix"
)

var (
	DefaultWebclient *http.Client
	RegexEmailLink   *regexp.Regexp

	ErrorDomainNotFound = errors.New("DFN: Domain not found or request id unknown")
)

func init() {
	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		log.Fatal(err)
	}

	DefaultWebclient = &http.Client{
		Jar:     jar,
		Timeout: time.Second * 30,
	}

	RegexEmailLink = regexp.MustCompile(`https://pki.pca.dfn.de/Eva/domain/(?P<raid>[0-9A-Za-z-]+)/(?P<token>[0-9A-Za-z-]+)`)
}

type HTTPError struct {
	error
	Status int
}

func findNamedMatches(regex *regexp.Regexp, str string) map[string]string {
	match := regex.FindStringSubmatch(str)

	results := map[string]string{}
	for i, name := range match {
		results[regex.SubexpNames()[i]] = name
	}
	return results
}

func DumpResponse(resp *http.Response) error {
	if resp == nil {
		return errors.New("Response argument is nil")
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	filebase := strings.ReplaceAll(
		strings.ReplaceAll(resp.Request.URL.String(),
			`:`, `_`),
		`/`, `_`)

	err = ioutil.WriteFile(filebase+".body", body, 0644)
	if err != nil {
		return err
	}

	jsonresp, err := json.Marshal(&resp.Header)
	err = ioutil.WriteFile(filebase+".header.json", jsonresp, 0644)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(filebase+".statuscode", []byte(strconv.Itoa(resp.StatusCode)), 0644)
	if err != nil {
		return err
	}

	return nil
}

func ExtractRATokenFromURL(url string) (string, string) {
	matches := findNamedMatches(RegexEmailLink, url)
	return matches["raid"], matches["token"]
}

func ActivateDomain(client *http.Client, url string) error {
	var (
		err error = nil
	)

	// get last part of URL from email
	raid, token := ExtractRATokenFromURL(url)
	if raid == "" || token == "" {
		return errors.New(fmt.Sprintf("Unable to extract relevant parts of URL: »%s«", url))
	}

	// construct activation URL
	activationUrl := fmt.Sprintf(`https://pki.pca.dfn.de/Eva/domainconfirm/%s/%s`, raid, token)

	// fetch activation URL
	resp, err := client.Get(activationUrl)
	if err != nil {
		return err
	}

	/*
		DumpResponse(resp)
		if err != nil {
			return err
		}
	*/

	// close body io.Reader
	defer func() {
		err := resp.Body.Close()
		if err != nil {
			fmt.Print(err)
		}
	}()

	// break on server error
	if resp.StatusCode != 200 {
		return HTTPError{
			error:  err,
			Status: resp.StatusCode,
		}
	}

	// read body
	bodybuffer := new(bytes.Buffer)
	_, err = bodybuffer.ReadFrom(resp.Body)
	if err != nil {
		return err
	}
	body := bodybuffer.String()

	if strings.Contains(body, "DOMAIN-NOT-FOUND") {
		return ErrorDomainNotFound
	}

	// known responses:
	// SERVER-DOMAIN-CONFIRMED, EMAIL-HOST-DOMAIN-CONFIRMED, EMAIL-DOMAIN-CONFIRMED
	if strings.Contains(body, "DOMAIN-CONFIRMED") {
		return nil
	}

	return errors.Errorf("DFN: request was sent but response was unclear: »%s«", base64.StdEncoding.EncodeToString(bodybuffer.Bytes()))
}
